package com.belkevich.exporter.prometheus.metricsgeneratorexporter.service;

import com.belkevich.exporter.prometheus.metricsgeneratorexporter.client.MetricsGeneratorClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MetricsCollectorAndTransformerToPrometheusTemplate {

  private final MetricsGeneratorClient metricsGeneratorClient;

  @Autowired
  public MetricsCollectorAndTransformerToPrometheusTemplate(
      MetricsGeneratorClient metricsGeneratorClient) {
    this.metricsGeneratorClient = metricsGeneratorClient;
  }

  public String collect() {
    Integer integerValue = metricsGeneratorClient.getValue(Integer.class);
    Double doubleValue = metricsGeneratorClient.getValue(Double.class);
    return transformMetrics(integerValue, doubleValue);
  }

  //master commit
  //master commit

  @SafeVarargs
  private final <T extends Number> String transformMetrics(T... metrics) {
    StringBuilder sb = new StringBuilder();
    for (T metric : metrics) {
      sb.append(metricTemplate(metric));
    }
    return sb.toString();
  }

  private <T extends Number> String metricTemplate(T metric) {
    return ("# HELP name_value A name value." + "\n" +
        "# TYPE name_value gauge" + "\n" +
        String.format("name_value %s", metric) + "\n").replace("name", getMetricName(metric));
  }

  private <T extends Number> String getMetricName(T metric) {
    return metric.getClass().getSimpleName().toLowerCase();
  }
}
