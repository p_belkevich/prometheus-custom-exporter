package com.belkevich.exporter.prometheus.metricsgeneratorexporter.controller;

import com.belkevich.exporter.prometheus.metricsgeneratorexporter.service.MetricsCollectorAndTransformerToPrometheusTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExporterController {

  private final MetricsCollectorAndTransformerToPrometheusTemplate metricsCollectorAndTransformerToPrometheusTemplate;


  @Autowired
  public ExporterController(
      MetricsCollectorAndTransformerToPrometheusTemplate metricsCollectorAndTransformerToPrometheusTemplate) {
    this.metricsCollectorAndTransformerToPrometheusTemplate = metricsCollectorAndTransformerToPrometheusTemplate;
  }

  @RequestMapping(
      method = RequestMethod.GET,
      produces = "text/plain; version=0.0.4",
      path = "/export")
  public String exportTransformedMetrics() {
    return metricsCollectorAndTransformerToPrometheusTemplate.collect();
  }
}
