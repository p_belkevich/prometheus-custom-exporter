package com.belkevich.exporter.prometheus.metricsgeneratorexporter.client;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class MetricsGeneratorClient {

  private static final String INTEGER_VALUES = "/integer";
  private static final String DOUBLE_VALUES = "/double";
  private static final String SIMPLE_GENERATOR = "/simple";
  private static final String COMPLEX_GENERATOR = "/complex";
  private static final String SIMPLE_GENERATOR_URL_TEMPLATE = "%s:%s%s";
  private static final String GENERATOR_URL_TEMPLATE = "%s%s";
  private static final Map<Class<?>, String> VALUES_MAP = new HashMap<>();

  static {
    VALUES_MAP.put(Integer.class, INTEGER_VALUES);
    VALUES_MAP.put(Double.class, DOUBLE_VALUES);
  }

  private final RestTemplate restTemplate;

  private final MetricsGeneratorProperties metricsGeneratorProperties;

  @Autowired
  public MetricsGeneratorClient(MetricsGeneratorProperties metricsGeneratorProperties,
      RestTemplate restTemplate) {
    this.metricsGeneratorProperties = metricsGeneratorProperties;
    this.restTemplate = restTemplate;
  }

  public <T extends Number> T getValue(Class<T> tClass) {
    String url = String
        .format(GENERATOR_URL_TEMPLATE, createSimpleGeneratorUrl(), VALUES_MAP.get(tClass));
    return restTemplate.getForEntity(url, tClass).getBody();
  }

  private String createSimpleGeneratorUrl() {
    return String
        .format(SIMPLE_GENERATOR_URL_TEMPLATE, metricsGeneratorProperties.getHost(),
            metricsGeneratorProperties.getPort(),
            SIMPLE_GENERATOR);
  }
}
